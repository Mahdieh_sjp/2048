<img src = "pic.png" alt="2048" style="width:100%;">


# 2048 Lite

Simpler version of Instagram witch is built with [**maven framwork**](https://maven.apache.org/) (software project management)

## Maintainer

- [**Mahdieh Sajedipoor**](https://gitlab.com/Mahdieh_sjp)


## Motivation
2048 lite is written by me and it is simplified version of 2048 game which is one of the popular games. I have done this as one of my programming project at Shahid Beheshti University.


## Mentors
A special thanks to
- [**Ali Alibadi**](https://gitlab.com/ali-aliabadi)
