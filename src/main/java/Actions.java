import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Actions {

    long score;
    boolean won;
    static long[][] table;

    Actions(long[][] table){
        score = 0;
        won = false;
        this.table = table;
    }

    public void actionUp(){

        long[][] temp = new long[4][4];

        for (int j = 0 ; j < 4 ; j++)
        {
            for (int i = 0; i < 4; i++)
                temp[i][j] = table[i][j];
            for (int i = 0; i < 3; i++) {

                fixU(j);

                if (table[i][j] == table[i + 1][j]) {
                    table[i][j] *= 2;
                    table[i + 1][j] = 0;
                    score += table[i][j];
                    if (table[i][j] == 2048)
                        won = true;
                }
            }
        }

        for (int j = 0 ; j < 4 ; j++)
            for (int i = 0; i < 4; i++) {
                if (table[i][j] != temp[i][j]){
                    newTile();
                    return;
                }
            }

    }

    public void fixU(int j) {
        Queue<Long> q = new LinkedList();

        for (int i = 0 ; i < 4; i++) {
            if(table[i][j] != 0)
                q.add(table[i][j]);
        }

        for (int i = 0 ; i < 4; i++) {
            if(!q.isEmpty())
                table[i][j] = q.poll();

            else table[i][j] = 0;
        }
    }

    public void actionDown(){

        long[][] temp = new long[4][4];

        for (int j = 0 ; j < 4 ; j++)
        {
            for (int i = 0; i < 4; i++)
                temp[i][j] = table[i][j];
            for (int i = 3; i > 0; i--) {

                fixD(j);

                if (table[i][j] == table[i - 1][j]) {
                    table[i][j] *= 2;
                    table[i - 1][j] = 0;
                    score += table[i][j];
                    if (table[i][j] == 2048)
                        won = true;
                }
            }
        }
        for (int j = 0 ; j < 4 ; j++)
            for (int i = 0; i < 4; i++) {
                if (table[i][j] != temp[i][j]){
                    newTile();
                    return;
                }
            }
    }

    public void fixD(int j) {
        Queue<Long> q = new LinkedList();

        for (int i = 3 ; i >= 0; i--) {
            if(table[i][j] != 0)
                q.add(table[i][j]);
        }

        for (int i = 3 ; i >= 0; i--) {
            if(!q.isEmpty())
                table[i][j] = q.poll();

            else table[i][j] = 0;
        }
    }

    public void actionRight(){

        long[][] temp = new long[4][4];

        for (int i = 0 ; i < 4 ; i++)
        {
            for (int j = 0; j < 4; j++)
                temp[i][j] = table[i][j];

            for (int j = 3; j > 0; j--) {

                fixR(i);

                if (table[i][j] == table[i][j - 1]) {
                    table[i][j] *= 2;
                    table[i][j - 1] = 0;
                    score += table[i][j];
                    if (table[i][j] == 2048)
                        won = true;
                }
            }
        }
        for (int j = 0 ; j < 4 ; j++)
            for (int i = 0; i < 4; i++) {
                if (table[i][j] != temp[i][j]){
                    newTile();
                    return;
                }
            }
    }

    public void fixR(int i) {
        Queue<Long> q = new LinkedList();

        for (int j = 3 ; j >= 0; j--) {
            if(table[i][j] != 0)
                q.add(table[i][j]);
        }

        for (int j = 3 ; j >= 0; j--) {
            if(!q.isEmpty())
                table[i][j] = q.poll();

            else table[i][j] = 0;
        }
    }

    public void actionLeft(){

        long[][] temp = new long[4][4];

        for (int i = 0 ; i < 4 ; i++) {
            for (int j = 0; j < 4; j++)
                temp[i][j] = table[i][j];

            for (int j = 0; j < 3; j++) {

                fixL(i);

                if (table[i][j] == table[i][j + 1]) {
                    table[i][j] *= 2;
                    table[i][j + 1] = 0;
                    score += table[i][j];
                    if (table[i][j] == 2048)
                        won = true;
                }
            }
        }
        for (int j = 0 ; j < 4 ; j++)
            for (int i = 0; i < 4; i++) {
                if (table[i][j] != temp[i][j]){
                    newTile();
                    return;
                }
            }
    }

    public void fixL(int i) {
        Queue<Long> q = new LinkedList();

        for (int j = 0 ; j < 4; j++) {
            if(table[i][j] != 0)
                q.add(table[i][j]);
        }

        for (int j = 0 ; j < 4; j++) {
            if(!q.isEmpty())
                table[i][j] = q.poll();

            else table[i][j] = 0;
        }
    }

    public void newTile(){

        Random random = new Random();
        int[] available = {2, 4};
        int i, j;
        do
        {
            i = random.nextInt(4);
            j = random.nextInt(4);
        }
        while( table[i][j] != 0 );
        table[i][j] = available[random.nextInt(available.length)];

    }

    public static boolean gameOver(){

        for (long[] i : table)
            for (long j : i)
                if(j == 0)
                    return false;

        for (int i = 0 ; i < 4 ; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == table[i][j + 1])
                    return false;
            }
        }

        for (int i = 0 ; i < 3 ; i++) {
            for (int j = 0; j < 4; j++) {
                if (table[i][j] == table[i + 1][j])
                    return false;
            }
        }

        return true;
    }


}
