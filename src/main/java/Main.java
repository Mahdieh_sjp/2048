import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String args[]){

        long[][] table = generateTable();
        start(table);
    }

    static long[][] generateTable(){

        long[][] table = new long[4][4];
        Random random = new Random();
        int[] available = {2, 4};
        int i1 = random.nextInt(4), j1 = random.nextInt(4), i2, j2;

        do
        {
            i2 = random.nextInt(4);
            j2 = random.nextInt(4);

        } while(i1 == i2 || j1 == j2);



        table[i1][j1] = available[random.nextInt(available.length)];
        table[i2][j2] = available[random.nextInt(available.length)];

        return table;
    }

    public static void start(long[][] table) {

        Actions action = new Actions(table);
        Scanner scanner = new Scanner(System.in);
        boolean gameOver = false;
        print(action);


        while(!gameOver){

            char inp = scanner.next().toLowerCase().charAt(0);

            if (inp == 'w')
                action.actionUp();
            else if(inp == 's')
                action.actionDown();
            else if(inp == 'd')
                action.actionRight();
            else if(inp == 'a')
                action.actionLeft();

            gameOver = action.gameOver();
            print(action);

        }

        System.out.println("GAME OVER!");

    }

    public static void print(Actions action) {

        clearScreen();

        System.out.println("-------------------------------------");
        for (long[] i : action.table)
            System.out.printf("|%-8d|%-8d|%-8d|%-8d|\n" +
                            "-------------------------------------\n"
                    , i[0], i[1], i[2], i[3]);

        if (action.won){
            System.out.println("\n\nCongratulations! You Won!");
            action.won = false;
        }
        System.out.println("\n\nyour score: " + action.score);

    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}

